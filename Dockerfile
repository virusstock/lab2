FROM ubuntu:latest

COPY lab1.deb /tmp/

RUN apt-get update && apt-get install -y /tmp/lab1.deb

CMD ["/usr/bin/lab1"]