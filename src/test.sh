#!/bin/bash

resultat="1 2 3 "
output=$(echo "3 2 3 1" | ./lab1)
sorted_array=$(echo "$output" | tail -n1) 

if [ "$resultat" == "$sorted_array" ]; then
    echo "Ura"
else
    echo "Otstoilo"
fi

resultat="21 33 43 77 107 "
output=$(echo "5 33 107 77 21 43" | ./lab1)
sorted_array=$(echo "$output" | tail -n1) 

if [ "$resultat" == "$sorted_array" ]; then
    echo "Ura"
else
    echo "Otstoilo"
fi

resultat="5 5 6 6 "
output=$(echo "4 5 6 5 6" | ./lab1)
sorted_array=$(echo "$output" | tail -n1) 

if [ "$resultat" == "$sorted_array" ]; then
    echo "Ura"
else
    echo "Otstoilo"
fi
